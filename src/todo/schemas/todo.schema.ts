import * as mongoose from 'mongoose';
const { Schema } = mongoose;

export const TodoSchema = new Schema({
    title: String,
    description: String,
    completed: Boolean
})