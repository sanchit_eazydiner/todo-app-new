import { Document } from 'mongoose';

export interface Todo extends Document {
   readonly title: String,
   readonly description: String,
   readonly completed: Boolean,
   readonly createdAt: Date
}