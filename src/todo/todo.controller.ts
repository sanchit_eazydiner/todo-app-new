import { Controller, Res, HttpStatus, Post, Get, Param, Body, Patch, Delete } from "@nestjs/common";
import { TodoService } from './todo.service';
import { CreateTodoDTO } from './dto/create-todo.dto';


@Controller('todo')
export class TodoController {
    constructor(private todoService: TodoService){}

    @Post('/add')
    async createATodo(@Res() res, @Body() createTodoDTO: CreateTodoDTO) {
        const todo = await this.todoService.createTodo(createTodoDTO);
        return res.status(HttpStatus.CREATED).json({
            status: 201,
            message: "Successful!",
            data: todo
        })
    }
    
    @Get('/all')
    async getAllTodos(@Res() res) {
        const todos = await this.todoService.getAllTodo();
        return res.status(HttpStatus.OK).json({
            status: 200,
            data: todos
        })
    }

    @Get("/:todoId")
    async getATodo(@Res() res, @Param("todoId") _id: string) {
        const todo = await this.todoService.getATodo(_id);
        if (!todo)
            return res
                .status(HttpStatus.NOT_FOUND)
                .json({ status: 404, error: "Not found!" });
        return res.status(HttpStatus.OK).json({ status: 200, data: todo });
    }

    @Patch('/update/:todoId')
    async updateCustomer(@Res() res, @Body() createTodoDTO: CreateTodoDTO, @Param("todoId") _id: string) {
        const todo = await this.todoService.updateATodo(_id, createTodoDTO);
        if (!todo)
            return res
                .status(HttpStatus.NOT_FOUND)
                .json({ status: 404, error: "Not found!" });
        return res.status(HttpStatus.OK).json({
            status: 200,
            message: 'Successful!',
            todo
        });
    }

    @Delete('/delete/:todoId')
    async deleteCustomer(@Res() res, @Param('todoId') _id) {
        const todo = await this.todoService.deleteATodo(_id);
        if (!todo)
            return res
                .status(HttpStatus.NOT_FOUND)
                .json({ status: 404, error: "Not found!" });
        return res.status(HttpStatus.OK).json({
            status: 200,
            message: 'Successful!',
        })
    }

}

