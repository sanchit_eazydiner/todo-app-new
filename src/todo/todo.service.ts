import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Todo } from './interface/todo.interface';
import {  CreateTodoDTO } from './dto/create-todo.dto';

@Injectable()
export class TodoService {
    constructor(@InjectModel("Todo") private readonly todoModel: Model<Todo>) { }

    async createTodo(createTodoDTO: CreateTodoDTO): Promise<Todo> {
        const newTodo = new this.todoModel(createTodoDTO);
        return await newTodo.save();
    }

    async getAllTodo(): Promise<Todo[]> {
        const todo = await this.todoModel.find().exec();
        return todo;
    }

    async getATodo(todoId): Promise<Todo> {
        const todo = await this.todoModel.findById(todoId).exec();
        return todo;
    }

    async updateATodo(_id, createTodoDTO: CreateTodoDTO): Promise<Todo> {
        const todo = await this.todoModel.findByIdAndUpdate(_id, createTodoDTO, { new: true });
        return todo;
    }

    async deleteATodo(_id): Promise<any> {
        const todo = await this.todoModel.findByIdAndRemove(_id);
        return todo;
    }
}