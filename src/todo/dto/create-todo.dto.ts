export class CreateTodoDTO {
    title: string;
    description: string;
    completed: boolean;
    createdAt: Date;
}  