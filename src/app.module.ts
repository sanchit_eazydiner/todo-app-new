import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {MongooseModule} from '@nestjs/mongoose';
import { TodoModule } from './todo/todo.module';
import "dotenv/config";

@Module({
  imports: [
    TodoModule,
    MongooseModule.forRoot('mongodb+srv://cluster0.imbou.mongodb.net/todoDB?retryWrites=true&w=majority',
  { 
    user: 'Sanchit', 
    pass: 'WinterX1',
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true, 
    useFindAndModify: false 
  }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
